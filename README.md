# Simulated Annealing

Simulated Annealing method implemented to find stable atom structures with the Lennard-Jones potential. 

This code was produced in 2012 as part of my undergrad studies in Computational Physics.
