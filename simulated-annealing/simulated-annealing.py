#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

    This file is part of Simulated-Annealing.

    Simulated-Annealing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PhyBio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simulated-Annealing.  If not, see <https://www.gnu.org/licenses/>.
    
@author: moreira
"""



from numpy import *  # this should be changed to import only the needed functions


def PBC(pos_matrix,structure_dimension):
    dr = 0.1
    i = 0
    while( i < pos_matrix.size/3.):

        if (absolute(pos_matrix.item((0,i))) > structure_dimension and pos_matrix.item((0,i)) > 0):
            pos_matrix.itemset((0,i), pos_matrix.item((0,i)) - dr) 
        elif ( absolute(pos_matrix.item( (0,i) ) ) > structure_dimension and pos_matrix.item((0,i)) < 0):
            pos_matrix.itemset((0,i), pos_matrix.item((0,i)) + dr ) 

        if (absolute(pos_matrix.item((1,i))) > structure_dimension and pos_matrix.item((1,i)) > 0):
            pos_matrix.itemset((1,i), pos_matrix.item((1,i)) - dr) 
        elif (absolute(pos_matrix.item((1,i))) > structure_dimension and pos_matrix.item((1,i)) < 0):
            pos_matrix.itemset((1,i), pos_matrix.item((1,i)) + dr) 

        if (absolute(pos_matrix.item((2,i)) )> structure_dimension and pos_matrix.item((2,i)) > 0):
            pos_matrix.itemset((2,i), pos_matrix.item((2,i)) - dr)
        elif (absolute(pos_matrix.item((2,i))) > structure_dimension and pos_matrix.item((2,i)) < 0):
            pos_matrix.itemset((2,i), pos_matrix.item((2,i))+ dr)
              
        i+=1
              
    return pos_matrix


def genLattice(lattice_parameter, structure_dimension, nAtoms):

    
    r = zeros((3,nAtoms))

    # freezing degrees of freedom for three particles
    
    r.itemset((0,0),0),r.itemset((1,0),0),r.itemset((2,0),0)        # particle 1
    r.itemset((0,1),structure_dimension/2.),r.itemset((1,1),0),r.itemset((2,1),0)      # particle 2  
    r.itemset((0,2),structure_dimension/2.),r.itemset((1,2),structure_dimension/2.),r.itemset((2,2),0)    # particle 3
    
    iParticle = 3
    
    while iParticle<nAtoms:

        x1,x2,x3 = random.uniform()*structure_dimension, random.uniform()*structure_dimension, random.uniform()*structure_dimension

        j = 0
        superposition = 0
        
        while (j<nAtoms):
            if (x1 == r.item((0,j)) and  x2 ==  r.item((1,j)) and x3 == r.item((2,j))):
                superposition += 1
            j+=1

        if (superposition == 0):
                r.itemset((0,iParticle), x1)
                r.itemset((1,iParticle), x2)
                r.itemset((2,iParticle), x3)
                               
                iParticle += 1
        
    return r       



def Energy_lj(pos_matrix):

    i = 0
    length = pos_matrix.size/3.
    Energy = 0.
    
    while (i<length):

        j = 0
        
        while (j<i):

            r_ij = matrix(( pos_matrix.item((0,i)) - pos_matrix.item((0,j)),pos_matrix.item((1,i)) - pos_matrix.item((1,j)), pos_matrix.item((2,i)) - pos_matrix.item((2,j)) ))
            r = r_ij.item(0)*r_ij.item(0) + r_ij.item(1)*r_ij.item(1) + r_ij.item(2)*r_ij.item(2)
            r_6 = r*r*r
            r_12 = r_6*r_6
            Energy += 4*(1./r_12 - 1./r_6)
                    
            j+=1
            
        i+=1

    return Energy


def displ(pos_matrix, gamma):
    fradius = 0.1
    new_matrix = zeros((3,pos_matrix.size/3.))
    new_matrix = new_matrix + pos_matrix
    
    old_energy = Energy_lj(pos_matrix)
    atom = random.randint(1,pos_matrix.size/3.)
    
    dx = gamma*(2.*random.uniform()-1.0)*fradius
    dy = gamma*(2.*random.uniform()-1.0)*fradius
    dz = gamma*(2.*random.uniform()-1.0)*fradius

    if( atom==1 ):
        new_matrix.itemset((0,atom), pos_matrix.item(0,atom) + dx)
    elif(atom==2):
        new_matrix.itemset((0,atom), pos_matrix.item(0,atom) + dx)
        new_matrix.itemset((1,atom), pos_matrix.item(1,atom) + dy)
    else:
        new_matrix.itemset((0,atom), pos_matrix.item(0,atom) + dx)
        new_matrix.itemset((1,atom), pos_matrix.item(1,atom) + dy)
        new_matrix.itemset((2,atom), pos_matrix.item(2,atom) + dz)
    
    new_energy = Energy_lj(new_matrix)
    dU = new_energy - old_energy

    if (dU<0):
        return new_matrix
    
    else:

        boltzmann_weight = exp(-dU/temperature)
        random_number = random.uniform()
            
        if (boltzmann_weight>random_number):
            return new_matrix

        else:
            return pos_matrix

    