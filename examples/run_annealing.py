#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

    This file is part of Simulated-Annealing.

    Simulated-Annealing is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PhyBio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simulated-Annealing.  If not, see <https://www.gnu.org/licenses/>.
    
@author: moreira
"""

import sys
sys.path.insert(0, '../simulated-annealing.py')
from annealing import *
from numpy import *


nAtoms = 32
lattice_parameter = 1.0
structure_dimension = 14
    
r = genLattice(lattice_parameter, structure_dimension, nAtoms)

E = Energy_lj(r)
print("initial energy:", E)

minimum_energy = E
gamma = 1.
Energy = []

max_iteration = 100
j=0
while(minimum_energy>-20.):

    temperature = 1e6  
    alpha = 0.9
    termal_steps = 0
    
    while (temperature>1e2):


        r = displ(r,gamma)
        r = PBC(r, structure_dimension)

        E = Energy_lj(r)
        
        if (E<minimum_energy):
            Energy.append(E)
            minimum_energy = E
            r_min = 1.*r
          
        if(termal_steps>100):
            termal_steps = 0
            temperature *= alpha
            gamma *= alpha**0.5
            
        termal_steps +=1

        
    print(r_min)
    print(min(Energy))
    gamma = 0.4
    r = 1.*r_min

print(r_min)
print(Energy_lj(r))